package com.Forgeware.InTime.Handlers;

import com.Forgeware.InTime.Time.Time;

public class ClockHandler {
	
public static String Clock(Time playerTime) {
		
		return playerTime.getYears() + ":" + playerTime.getDays() +":" + playerTime.getHours() +":" + playerTime.getMinutes() +":" + playerTime.getSeconds();
	

	}

}
