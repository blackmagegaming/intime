package com.Forgeware.InTime.Handlers;

import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import com.Forgeware.InTime.Time.Time;

public class TimeRecievedHandler implements IMessageHandler<TimeMessage, IMessage> {

	public TimeRecievedHandler()
	{
		
	}
	
	public IMessage onMessage(TimeMessage message, MessageContext ctx) {
		Time t = message.getTime();
		
		System.out.println("Time left is " + t.getHours() + "hrs, " + t.getMinutes() + "mins, " + t.getSeconds() + "secs.");
		//InfoLine.RenderOntoHUD(ctx.getServerHandler().playerEntity, t);
		
		return null;
	}

}
