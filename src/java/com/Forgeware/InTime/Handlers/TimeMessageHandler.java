package com.Forgeware.InTime.Handlers;

import com.Forgeware.InTime.Time.Time;
import com.Forgeware.InTime.Time.TimeManager;

import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class TimeMessageHandler implements IMessageHandler<TimeSyncMessage, TimeMessage> {

	public TimeMessageHandler()
	{
		
	}
	
	public TimeMessage onMessage(TimeSyncMessage message, MessageContext ctx) {
		Time t = TimeManager.Query(ctx.getServerHandler().playerEntity.getUniqueID());
		return new TimeMessage(t);
	}

}
