package com.Forgeware.InTime.Handlers;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

import com.Forgeware.InTime.Time.Time;

public class TimeMessage implements IMessage {

	private Time timeObj;
	
	public TimeMessage()
	{
		
	}
	
	public TimeMessage(Time t) {
		this.timeObj = t;
	}
	
	public void fromBytes(ByteBuf buf) {
		// Deseralized data
		int secs, mins, hrs, days, years;
		long tick;
		boolean paused;
		
		years = buf.readInt();
		days = buf.readInt();
		hrs = buf.readInt();
		mins = buf.readInt();
		secs = buf.readInt();
		paused = buf.readBoolean();
		tick = buf.readLong();
		
		timeObj = new Time(secs, mins, hrs, days, years);
		timeObj.setTick(tick);
		timeObj.setPaused(paused);
	}

	public void toBytes(ByteBuf buf) {
		// Deserialize data
		buf.writeInt(this.timeObj.getYears());
		buf.writeInt(this.timeObj.getDays());
		buf.writeInt(this.timeObj.getHours());
		buf.writeInt(this.timeObj.getMinutes());
		buf.writeInt(this.timeObj.getSeconds());
		buf.writeBoolean(this.timeObj.isPaused());
		buf.writeLong(this.timeObj.getLastTick());
	}
	
	public Time getTime()
	{
		return this.timeObj;
	}
	
}
