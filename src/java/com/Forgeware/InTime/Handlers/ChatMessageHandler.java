package com.Forgeware.InTime.Handlers;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;

import com.Forgeware.InTime.InTime;

public class ChatMessageHandler {
	
	private static final ITextComponent EIChatComponent = createEIChatComponent(InTime.MODID.toUpperCase());

    public static void icommandsenderReply(ICommandSender player, String message) {
        sendEIChatToPlayer((EntityPlayer)player, message);
    }

    private static ITextComponent createEIChatComponent(String string) {
        TextComponentString EIComponent = new TextComponentString(string);
          return EIComponent;
    }

    public static ITextComponent createChatComponent(String message) {
        TextComponentString component = new TextComponentString(message);
        return EIChatComponent.appendSibling(component);
    }

    public static void sendEIChatToPlayer(EntityPlayer player, String message) {
        player.addChatComponentMessage(createChatComponent(message));
    }

    public static void broadcastEIMessageToPlayers(String message){
        MinecraftServer.getServer().getConfigurationManager().sendChatMsg(createChatComponent(message));
    }

}
