package com.Forgeware.InTime.Handlers;

import java.util.ArrayList;

import com.Forgeware.InTime.Time.Time;
import com.Forgeware.InTime.Time.TimeManager;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.player.PlayerPickupXpEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.PlayerTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;

public class TimeModifyHandler {
	
	@SubscribeEvent
	public void PlayerLoggedIn(PlayerLoggedInEvent event)
	{
		Time t = TimeManager.Query(event.player.getUniqueID());
		
		if(t != null)
		{
			// TODO: Check for time cards.
			if(!t.isDepleted())
			{
				// FIXME: Remove call to setTick() and place in PauseTime()
				t.setTick(event.player.worldObj.getWorldTime());
				TimeManager.PauseTime(event.player.getUniqueID(), false);
			} else {
				// TODO: Kick Player from server			
			}
		} else {
			TimeManager.Track(event.player.getUniqueID());
			TimeManager.Query(event.player.getUniqueID()).setTick(event.player.worldObj.getWorldTime());
		}
	}
	
	@SubscribeEvent
	public void PlayerLoggedOut(PlayerLoggedOutEvent event)
	{
		TimeManager.PauseTime(event.player.getUniqueID(), true);
	}
	
	@SubscribeEvent
	public void onWorldTick(WorldTickEvent event)
	{
		ArrayList<Time> times = TimeManager.GetActivePlayers();
		for(Time t: times)
		{
			if(t.isDepleted())
			{
				t.setPaused(true);
			}
			
			long tick = calculateElapsedTick(t.getLastTick(), event.world.getWorldTime());
			
			if(tick == 20)
			{
				t.removeSeconds(1);
				t.setTick(t.getLastTick() + tick);
			}
		}				
	}
	
	private long calculateElapsedTick(long playerLastTick, long worldLastTick)
	{
		if(playerLastTick > worldLastTick)
		{
			return (24000L - playerLastTick) + worldLastTick;
		} else
			return worldLastTick - playerLastTick;
	}
	
	@SubscribeEvent
	public void onPlayerKill(PlayerTickEvent event)
	{
		Time t = TimeManager.Query(event.player.getUniqueID());
		
		if(t != null && t.isDepleted())
		{
			// TODO: Custom Kill Message
			//event.player.onKillCommand();
			event.player.inventory.dropAllItems();
			
			if(!event.player.isDead)
			{
				event.player.isDead = true;
				ChatMessageHandler.broadcastEIMessageToPlayers(event.player.getName() + " has run out of time and perished!");
			}
				
			// TODO: Broadcast a message that they ran out of time.
			// TODO: Teleport to jail system/
		}
	}
	
	@SubscribeEvent
	public void onXPPickUp(PlayerPickupXpEvent event)
	{
		int amount = event.orb.getXpValue();
		TimeManager.IncreaseTime(event.entityPlayer.getUniqueID(), new Time(0, amount, 0, 0, 0));
	}
	
	@SubscribeEvent
	public void onPlayerKillMob(LivingDeathEvent event)
	{
		if(event.entityLiving instanceof EntityPlayer)
		{
			// TODO: Adjust punishment/rewards
			EntityPlayer player = (EntityPlayer) event.entityLiving;
			TimeManager.DecreaseTime(player.getUniqueID(), new Time(0, 20, 0, 0, 0));
			
			if(event.source.getEntity() instanceof EntityPlayer)
			{
				EntityPlayer src = (EntityPlayer) event.source.getEntity();
				TimeManager.IncreaseTime(src.getUniqueID(), new Time(0, 20, 0, 0, 0));
			}			
		} else {
			if(event.source.getEntity() instanceof EntityPlayer)
			{
				EntityPlayer src = (EntityPlayer) event.source.getEntity();
				TimeManager.IncreaseTime(src.getUniqueID(), new Time(0, 20, 0, 0, 0));
			}
		}
	}
}
