package com.Forgeware.InTime.Time;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import com.Forgeware.InTime.InTime;

public final class TimeManager {

	private static boolean init = false;
	private static HashMap<UUID, Time> timeDB = null;
	
	/*
	 * Description: The private constructor for this class. Completely placeholder to prevent other classes from using it.
	 */
	private TimeManager()
	{
		
	}
	
	/*
	 * Description: This method initializes the singleton instance of the time manager. Should only be called once per mod run.
	 */
	public static void Init()
	{
		if(!init) {
			TimeManager.timeDB = new HashMap<UUID, Time>();
			TimeManager.init = true;
		}
		else
			System.out.println("WARNING: Tried to init TimeManager after it was already init'd!");
	}

	/*
	 * Description: This method finds the time remaining for a specific player.
	 */
	public static Time Query(UUID playerID)
	{
		if(init)
			return timeDB.get(playerID);
		else return null;
	}

	/*
	 * Description: This method adds a player to the time manager and tracks the time the player has remaining.
	 */
	public static void Track(UUID playerID)
	{
		if(!init) return;
		
		Time t = TimeManager.Query(playerID);
		
		// Player is not in the Time DB
		if(t == null){
			timeDB.put(playerID, InTime.getDefaultTime());
		}
	}
	
	/*
	 * Description: This method will decrease the time of the specific player.
	 */
	public static void DecreaseTime(UUID playerID, Time amount)
	{
		if(!init) return;
		
		Time t = TimeManager.Query(playerID);
		
		if(t != null)
		{
			t.removeTime(amount);
			if(t.isDepleted())
			{
				// TODO: Kick player from the server, and prevent them from rejoining.
			}
		}
	}
	
	/*
	 * Description: This method will increase the time of the specified player.
	 */
	public static void IncreaseTime(UUID playerID, Time amount)
	{
		if(!init) return;
		
		Time t = TimeManager.Query(playerID);
		
		if(t != null)
		{
			t.addTime(amount);
			if(t.isDepleted())
			{
				// TODO: Kick player from the server, and prevent them from rejoining.
			} else {
				// TODO: Make sure that the player can rejoin the server
			}
		}
	}
	
	/**Description: This method will pause the time of the specified player.*/
	public static void PauseTime(UUID playerID, boolean pauseValue)
	{
		if(!init) return;
		
		Time t = TimeManager.Query(playerID);
		
		if(t != null)
		{
			t.setPaused(pauseValue);
		}
	}
	
	/**Description: This method will pause the time of the specified player.*/
	public static boolean getPaused(UUID playerID)
	{
		if(!init) {}else{
			Time t = TimeManager.Query(playerID);
			
			if(t != null)
			{
				return t.isPaused();
			}	
			
		}
		
		return false;
	}

	public static ArrayList<Time> GetActivePlayers()
	{
		ArrayList<Time> players = new ArrayList<Time>();
		for(UUID id : timeDB.keySet())
		{
			if(TimeManager.Query(id) != null && !TimeManager.Query(id).isPaused())
			{
				players.add(timeDB.get(id));
			}
		}
		
		return players;
	}
	
	public static void setDefaultTime(UUID playerID){
		TimeManager.Query(playerID).setTime(InTime.getDefaultTime());
		System.out.println("Setting Default");
		
	}

}
