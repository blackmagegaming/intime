package com.Forgeware.InTime.Time;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.UUID;

public interface ITimeDatabaseConnector {
	void Connect(String url) throws ClassNotFoundException;
	void CreateDatabase(String url);
	ResultSet Read();
	void Write(HashMap<UUID, Time> timeMap);
	void Disconnect();
}
