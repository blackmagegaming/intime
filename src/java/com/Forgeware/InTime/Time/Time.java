package com.Forgeware.InTime.Time;

public class Time {
	// Number of Years
	private int years;
	
	// Number of Days
	private int days;
	
	// Number of hours
	private int hours;
	
	// Number of minutes
	private int minutes;
	
	// Number of seconds
	private int seconds;
	
	private boolean paused = false;
	private long lastTick;
	
	// Default Constructor
	public Time()
	{
		years = 0;
		days = 0;
		hours = 0;
		minutes = 0;
		seconds = 0;
	}
	
	public Time(int seconds, int minutes, int hours, int days, int years)
	{
		this.seconds = seconds;
		this.minutes = minutes;
		this.hours = hours;
		this.days = days;
		this.years = years;
	}

	/* Setters */
	protected void setTime(int seconds, int minutes, int hours, int days, int years)
	{
		this.seconds = seconds;
		this.minutes = minutes;
		this.hours = hours;
		this.days = days;
		this.years = years;
	}
	
	protected void setSeconds(int seconds)
	{
		this.seconds = seconds;
	}
	
	protected void setMinutes(int minutes)
	{
		this.minutes = minutes;
	}
	
	protected void setHours(int hours)
	{
		this.hours = hours;
	}
	
	protected void setDays(int days)
	{
		this.days = days;
	}
	
	protected void setYears(int years)
	{
		this.years = years;
	}

	public void setTime(Time t)
	{
		this.seconds = t.seconds;
		this.minutes = t.minutes;
		this.hours = t.hours;
		this.days = t.days;
		this.years = t.years;
	}
	
	public void setPaused(boolean value)
	{
		this.paused = value;
	}
	
	public void setTick(long tick)
	{
		this.lastTick = tick;
	}
	
	/* Getters */
	public int getSeconds()
	{
		return this.seconds;
	}
	
	public int getMinutes()
	{
		return this.minutes;
	}
	
	public int getHours()
	{
		return this.hours;
	}
	
	public int getDays()
	{
		return this.days;
	}
	
	public int getYears()
	{
		return this.years;
	}

	
	public boolean isPaused()
	{
		return this.paused;
	}
	
	public long getLastTick()
	{
		return this.lastTick;
	}
	
	public boolean isDepleted()
	{
		boolean depleted = this.seconds <= 0 && this.minutes <= 0 && this.hours <= 0 && this.days <= 0 && this.years <= 0;
		if(depleted) 
		{
			this.seconds = 0;
			this.minutes = 0;
			this.hours = 0;
			this.days = 0;
			this.years = 0;
		}

		return depleted;
	}

	
	// Add operations
	public void addTime(Time t)
	{
		addSeconds(t.getSeconds());
		addMinutes(t.getMinutes());
		addHours(t.getHours());
		addDays(t.getDays());
		addYears(t.getYears());
	}
	
	public void addSeconds(int seconds)
	{
		for(int i = 0; i < seconds; i++)
		{
			if(this.seconds == 59)
			{
				this.seconds = 0;
				addMinutes(1);
			} else {
				this.seconds++;
			}
		}
		return;
	}
	
	public void addMinutes(int minutes)
	{
		for(int i = 0; i < minutes; i++)
		{
			if(this.minutes == 59)
			{
				this.minutes = 0;
				addHours(1);
			} else {
				this.minutes++;
			}
		}
		return;
	}
	
	public void addHours(int hours)
	{
		for(int i = 0; i < hours; i++)
		{
			if(this.hours == 23)
			{
				this.hours = 0;
				addDays(1);
			} else {
				this.hours++;
			}
		}
	}

	public void addDays(int days)
	{
		for(int i = 0; i < days; i++)
		{
			if(this.days == 364)
			{
				this.days = 0;
				addYears(1);
			} else {
				this.days++;
			}
		}
	}
	
	// TODO: Should we really worry about overflow here?
	public void addYears(int years)
	{
		this.years += years;
	}
	
	
	// Remove operations
	// TODO: Might need to improve arithmetic to determine zero times
	public void removeTime(Time t)
	{
		removeYears(t.getYears());
		removeDays(t.getDays());
		removeHours(t.getHours());
		removeMinutes(t.getMinutes());
		removeSeconds(t.getSeconds());
	}
	
	public void removeSeconds(int seconds)
	{
		for(int i = 0; i < seconds; i++)
		{
			if(this.seconds == 0)
			{
				this.seconds = 59;
				removeMinutes(1);
			} else {
				this.seconds--;
			}
		}
	}
	
	public void removeMinutes(int minutes)
	{
		for(int i = 0; i < minutes; i++)
		{
			if(this.minutes == 0)
			{
				this.minutes = 59;
				removeHours(1);
			} else {
				this.minutes--;
			}
		}
	}
	
	public void removeHours(int hours)
	{
		for(int i = 0; i < hours; i++)
		{
			if(this.hours == 0)
			{
				this.hours = 23;
				removeDays(1);
			} else {
				this.hours--;
			}
		}
	}
	
	public void removeDays(int days)
	{
		for(int i = 0; i < days; i++)
		{
			if(this.days == 0)
			{
				this.days = 364;
				this.removeYears(1);
			} else {
				this.days--;
			}
		}
	}
	
	public void removeYears(int years)
	{
		if(this.years > 0)
			this.years--;
	}
}
