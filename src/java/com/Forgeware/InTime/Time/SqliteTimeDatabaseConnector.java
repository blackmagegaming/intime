/*
 * SqliteTimeDatabaseConnector.java
 * 
 * Description: SQLite implementation for the Time database in InTime. This contains code which will
 * connect, update, and read from an sqlite database. We are using sqlite-jdbc for connecting to the
 * sqlite database. For more information please visit: https://bitbucket.org/xerial/sqlite-jdbc/src
 * 
 */
package com.Forgeware.InTime.Time;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.UUID;

public class SqliteTimeDatabaseConnector implements ITimeDatabaseConnector {

	private Connection connection = null;
	
	/*
	 * Description: Opens a connection to a SQLite Database.
	 */
	@Override
	public void Connect(String url) throws ClassNotFoundException
	{
		Class.forName("org.sqlite.JDBC");
		
		try {
			connection = DriverManager.getConnection("jdbc:sqlite:" + url);
			Statement s = connection.createStatement();
			s.setQueryTimeout(30); // 30 second timeout
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	/* 
	 * Description: Reads an entire SQLite database, and returns the ResultSet object for further processing.
	 */
	@Override
	public ResultSet Read() {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * Description: Writes out the entire timeDB containing the time data for each player.
	 */
	@Override
	public void Write(HashMap<UUID, Time> timeMap) {
		// TODO Auto-generated method stub

	}

	/*
	 * Description: Closes a currently connected SQLite database connection.
	 */
	@Override
	public void Disconnect() {
		// TODO Auto-generated method stub

	}

	/* 
	 * Description: Creates a new SQLite Time database for the specified database.
	 */
	@Override
	public void CreateDatabase(String filename) {
		// TODO Auto-generated method stub
		
	}

}
