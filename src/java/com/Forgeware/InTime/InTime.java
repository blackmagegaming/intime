package com.Forgeware.InTime;

import java.util.ArrayList;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.player.PlayerPickupXpEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.PlayerTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.WorldTickEvent;
import net.minecraftforge.fml.relauncher.Side;
import com.Forgeware.InTime.Commands.CommandCleanTime;
import com.Forgeware.InTime.Commands.CommandGiveTime;
import com.Forgeware.InTime.Commands.CommandPauseTime;
import com.Forgeware.InTime.Commands.CommandRemoveTime;
import com.Forgeware.InTime.Commands.CommandShowTime;
import com.Forgeware.InTime.HUD.HUDRender;
import com.Forgeware.InTime.Handlers.ChatMessageHandler;
import com.Forgeware.InTime.Handlers.InTimePacketHandler;
import com.Forgeware.InTime.Handlers.TimeMessage;
import com.Forgeware.InTime.Handlers.TimeMessageHandler;
import com.Forgeware.InTime.Handlers.TimeRecievedHandler;
import com.Forgeware.InTime.Handlers.TimeSyncMessage;
import com.Forgeware.InTime.Time.Time;
import com.Forgeware.InTime.Time.TimeManager;

@Mod(modid = InTime.MODID, name = InTime.Modname, dependencies = "required-after:Forge@["+ InTime.Forgeversion +",)")

public class InTime
{
  /* Version information consists of the following three values:
      Major Version
      Minor Version
      Revision
     This can be followed by a -a indicating Alpha or -b for Beta
     This system is to be followed in every update for easy referencing at later dates.
     The MODID, Modname, Forgeversion and Version are all controlled with the following Strings throughout the mod*/

    public static final String MODID = "intime";
    public static final String Modname = "InTime";
    public static final String Forgeversion = "12.16.0.1811";
    public static final String VERSION = "1.0.0-a";
	
	// something useful
	@SuppressWarnings("unused")
	private static long ticks;
	@SuppressWarnings("unused")
	private static final Time defTime = new Time(0, 0, 2, 0, 0);
	
	// TODO: Determine the default time value and return it
	public static Time getDefaultTime()
	{
		return new Time(0, 0, 2, 0, 0);
	}
	
	@EventHandler
	public void Load(FMLInitializationEvent event)
	{
		System.out.println("Initializing InTime Mod");
		System.out.println("By ForgeWare");
		TimeManager.Init();
		MinecraftForge.EVENT_BUS.register(new TestHandler());
		
		if(event.getSide() == Side.CLIENT)
			MinecraftForge.EVENT_BUS.register(HUDRender.instance);
		
		MinecraftForge.EVENT_BUS.register(new TestHandler());
		InTimePacketHandler.INSTANCE.registerMessage(TimeMessageHandler.class, TimeSyncMessage.class, 1, Side.SERVER);
		InTimePacketHandler.INSTANCE.registerMessage(TimeRecievedHandler.class, TimeMessage.class, 2, Side.CLIENT);
	}
	@EventHandler
	public void PostLoad(FMLPostInitializationEvent event){
		
	}
	
	@EventHandler
	public void serverLoad(FMLServerStartingEvent event){
		event.registerServerCommand(new CommandGiveTime());
		event.registerServerCommand(new CommandRemoveTime());
		event.registerServerCommand(new CommandPauseTime());
		event.registerServerCommand(new CommandCleanTime());
		event.registerServerCommand(new CommandShowTime());
	}
	
	public class TestHandler {
		
		@SubscribeEvent
		public void PlayerKill(PlayerTickEvent event)
		{
			Time t = TimeManager.Query(event.player.getUniqueID());
			
			if(t != null && t.isDepleted())
			{
				// TODO: Custom Kill Message
				//event.player.onKillCommand();
				/*ItemStack[] inv = event.player.inventory);
				
				for(ItemStack is : inv)
				{
					event.player.inventory.dropAllItems();
				}*/
				
				if(!event.player.isDead)
				{
					event.player.isDead = true;
					ChatMessageHandler.broadcastEIMessageToPlayers(event.player.getName() + " has run out of time and perished!");
				}
					
				// TODO: Broadcast a message that they ran out of time.
				// TODO: Teleport to jail system/
			}
		}
		
		private long calculateElapsedTick(long playerLastTick, long worldLastTick)
		{
			if(playerLastTick > worldLastTick)
			{
				return (24000L - playerLastTick) + worldLastTick;
			} else
				return worldLastTick - playerLastTick;
		}
		
		@SubscribeEvent
		public void onWorldTick(WorldTickEvent event)
		{
			// Increase the current tick amount
			//ticks = event.world.getWorldTime() / 1000;
			
			ArrayList<Time> times = TimeManager.GetActivePlayers();
			for(Time t: times)
			{
				if(t.isDepleted())
				{
					t.setPaused(true);
				}
				
				long tick = calculateElapsedTick(t.getLastTick(), event.world.getWorldTime());
				
				if(tick == 20)
				{
					t.removeSeconds(1);
					t.setTick(event.world.getWorldTime());
				}
			}
			
			
						
		}
		
		@SubscribeEvent
		public void PlayerLoggedIn(PlayerLoggedInEvent event)
		{
			Time t = TimeManager.Query(event.player.getUniqueID());
			
			if(t != null)
			{
				// TODO: Check for time cards.
				if(!t.isDepleted())
				{
					t.setPaused(false);
					t.setTick(event.player.worldObj.getWorldTime());
				} else {
					// TODO: Kick Player from server
				}
			} else {
				TimeManager.Track(event.player.getUniqueID());
				TimeManager.Query(event.player.getUniqueID()).setTick(event.player.worldObj.getWorldTime());
			}
		}
		
		@SubscribeEvent
		public void PlayerLoggedOut(PlayerLoggedOutEvent event)
		{
			@SuppressWarnings("unused")
			Time t = TimeManager.Query(event.player.getUniqueID());
			
			//t.setPaused(true);
		}
		
		@SubscribeEvent
		public void onXPPickUp(PlayerPickupXpEvent event)
		{
			int amount = event.getOrb().getXpValue();
			TimeManager.IncreaseTime(event.getEntityPlayer().getUniqueID(), new Time(0, amount, 0, 0, 0));
		}
		
		@SubscribeEvent
		public void onPlayerKillMob(LivingDeathEvent event)
		{
			if(event.getEntityLiving() instanceof EntityPlayer)
			{
				// TODO: Adjust punishment/rewards
				EntityPlayer player = (EntityPlayer) event.getEntityLiving();
				TimeManager.DecreaseTime(player.getUniqueID(), new Time(0, 20, 0, 0, 0));
				
				if(event.getSource().getEntity() instanceof EntityPlayer)
				{
					EntityPlayer src = (EntityPlayer) event.getSource().getEntity();
					TimeManager.IncreaseTime(src.getUniqueID(), new Time(0, 20, 0, 0, 0));
				}			
			} else {
				if(event.getSource().getEntity() instanceof EntityPlayer)
				{
					EntityPlayer src = (EntityPlayer) event.getSource().getEntity();
					TimeManager.IncreaseTime(src.getUniqueID(), new Time(0, 20, 0, 0, 0));
				}
			}
		}
	}
}