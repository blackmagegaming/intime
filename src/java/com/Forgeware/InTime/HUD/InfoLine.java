package com.Forgeware.InTime.HUD;

import net.minecraft.client.gui.GuiChat;
import net.minecraft.entity.player.EntityPlayer;


/**
 * The Info Line consists of everything that gets displayed in the top-left portion
 * of the screen. It's job is to gather information about other classes and render
 * their message into the Info Line.
 */
public class InfoLine extends HUDModBase
{
    
    public static boolean ShowBiome;
    public static boolean ShowCanSnow;
    
    /**  The padding string that is inserted between different elements of the Info Line */
    public static final String SPACER = " ";
    public static int infoLineLocX = 1;
    public static int infoLineLocY = 1;

    
    /** The notification string currently being rendered */
    public static String notificationMessage = "";
    
    /** The info line string currently being rendered */
    public static String infoLineMessage;
    

    /**
     * Renders the on screen message consisting of everything that gets put into the top let message area,
     * including coordinates and the state of things that can be activated
     */
    public static void RenderOntoHUD(EntityPlayer player)
    {
        //if the player is in the world
        //and not looking at a menu
        //and F3 not pressed
        if ((mc.inGameHasFocus || (mc.currentScreen != null && (mc.currentScreen instanceof GuiChat))) &&
                !mc.gameSettings.showDebugInfo)
        {
        	infoLineMessage = "";
        	
            String clock = HUDClock.CalculateMessageForInfoLine(infoLineMessage, player);
            if (clock.length() > 0)
            	clock += SPACER;
            infoLineMessage += clock;

            mc.fontRendererObj.drawStringWithShadow(infoLineMessage, infoLineLocX, infoLineLocY, 0x00ff00);
        }
    }
}