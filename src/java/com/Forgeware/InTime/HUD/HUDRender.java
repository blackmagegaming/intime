package com.Forgeware.InTime.HUD;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.*;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class HUDRender {

		public static final HUDRender instance = new HUDRender();
		private static Minecraft mc = Minecraft.getMinecraft();
		
		/**
		 * Event fired at various points during the GUI rendering process.
		 * We render anything that need to be rendered onto the HUD in this method.
		 * @param event
		 */
	    @SubscribeEvent
	    @SideOnly(Side.CLIENT)
	    public void RenderGameOverlayEvent(RenderGameOverlayEvent event)
	    {
	    	//render everything onto the screen
	    	
	    	if(event.getType() == RenderGameOverlayEvent.ElementType.TEXT)
	    	{
	    		InfoLine.RenderOntoHUD(mc.thePlayer);
	    		//DistanceMeasurer.RenderOntoHUD();
	            //DurabilityInfo.RenderOntoHUD();
	            //PotionTimers.RenderOntoHUD();
	            //HUDEntityTrackerHelper.RenderEntityInfo(event.partialTicks);	//calls other mods that need to render things on the HUD near entities
	            //ItemSelector.RenderOntoHUD(event.partialTicks);
	    	}
	    	
	    }
		
		/**
		 * Renders floating text in the 3D world at a specific position.
		 * @param text The text to render
		 * @param x X coordinate in the game world
		 * @param y Y coordinate in the game world
		 * @param z Z coordinate in the game world
		 * @param color 0xRRGGBB text color
		 * @param renderBlackBackground render a pretty black border behind the text?
		 * @param partialTickTime Usually taken from RenderWorldLastEvent.partialTicks variable
		 */
	    public static void RenderFloatingText(String text, float x, float y, float z, int color, boolean renderBlackBackground, float partialTickTime)
	    {
	    	String textArray[] = {text};
	    	RenderFloatingText(textArray, x, y, z, color, renderBlackBackground, partialTickTime);
	    }
	    
	    /**
		 * Renders floating lines of text in the 3D world at a specific position.
		 * @param text The string array of text to render
		 * @param x X coordinate in the game world
		 * @param y Y coordinate in the game world
		 * @param z Z coordinate in the game world
		 * @param color 0xRRGGBB text color
		 * @param renderBlackBackground render a pretty black border behind the text?
		 * @param partialTickTime Usually taken from RenderWorldLastEvent.partialTicks variable
		 */
	    @SuppressWarnings("unused")
	 // TODO  not sure we fixed correctly
		public static void RenderFloatingText(String[] text, float x, float y, float z, int color, boolean renderBlackBackground, float partialTickTime)
	    {
	    	//Thanks to Electric-Expansion mod for the majority of this code
	    	//https://github.com/Alex-hawks/Electric-Expansion/blob/master/src/electricexpansion/client/render/RenderFloatingText.java
	    	
	    	RenderManager renderManager = mc.getRenderManager();
	        
	        float playerX = (float) (mc.thePlayer.lastTickPosX + (mc.thePlayer.posX - mc.thePlayer.lastTickPosX) * partialTickTime);
	        float playerY = (float) (mc.thePlayer.lastTickPosY + (mc.thePlayer.posY - mc.thePlayer.lastTickPosY) * partialTickTime);
	        float playerZ = (float) (mc.thePlayer.lastTickPosZ + (mc.thePlayer.posZ - mc.thePlayer.lastTickPosZ) * partialTickTime);

	        float dx = x-playerX;
	        float dy = y-playerY;
	        float dz = z-playerZ;
	        float distance = (float) Math.sqrt(dx*dx + dy*dy + dz*dz);
	        float scale = 0.03f;
	        
	        GL11.glColor4f(1f, 1f, 1f, 0.5f);
	        GL11.glPushMatrix();
	        GL11.glTranslatef(dx, dy, dz);
	        GL11.glRotatef(-renderManager.playerViewY, 0.0F, 1.0F, 0.0F);
	        GL11.glRotatef(renderManager.playerViewX, 1.0F, 0.0F, 0.0F);
	        GL11.glScalef(-scale, -scale, scale);
	        GL11.glDisable(GL11.GL_LIGHTING);
	        GL11.glDepthMask(false);
	        GL11.glDisable(GL11.GL_DEPTH_TEST);
	        GL11.glEnable(GL11.GL_BLEND);
	        GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
	        
	        int textWidth = 0;
	        for (String thisMessage : text)
	        {
	            int thisMessageWidth = mc.fontRendererObj.getStringWidth(thisMessage);

	            if (thisMessageWidth > textWidth)
	            	textWidth = thisMessageWidth;
	        }
	        
	        int lineHeight = 10;
	        
	        if(renderBlackBackground)
	        {
	            Tessellator tessellator = Tessellator.getInstance();
	            VertexBuffer worldrenderer = tessellator.getBuffer();
	        	
	            GL11.glDisable(GL11.GL_TEXTURE_2D);
	           // worldrenderer.startDrawingQuads();
	            int stringMiddle = textWidth / 2;
	            GlStateManager.color(0.0F, 0.0F, 0.0F, 0.5F);
	           // worldrenderer.addVertex(-stringMiddle - 1, -1 + 0, 0.0D);
	           // worldrenderer.addVertex(-stringMiddle - 1, 8 + lineHeight*text.length-lineHeight, 0.0D);
	           // worldrenderer.addVertex(stringMiddle + 1, 8 + lineHeight*text.length-lineHeight, 0.0D);
	           // worldrenderer.addVertex(stringMiddle + 1, -1 + 0, 0.0D);
	            tessellator.draw();
	            GL11.glEnable(GL11.GL_TEXTURE_2D);
	        }
	        
	        int i = 0;
	        for(String message : text)
	        {
	        	mc.fontRendererObj.drawString(message, -textWidth / 2, i*lineHeight, color);
	        	i++;
	        }
	        
	        GL11.glColor4f(1.0F, 1.0F, 1.0F, 1.0F);
	        GL11.glDepthMask(true);
	        GL11.glEnable(GL11.GL_DEPTH_TEST);
	        GL11.glPopMatrix();
	    }
	    

	    /**
	     * Displays a short notification to the user. Uses the Minecraft code to display messages.
	     * @param message the message to be displayed
	     */
	    public static void DisplayNotification(String message)
	    {
	        mc.ingameGUI.setRecordPlaying(message, false);
	    }

}
