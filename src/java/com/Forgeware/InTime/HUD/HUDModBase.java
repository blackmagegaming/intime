package com.Forgeware.InTime.HUD;

import net.minecraft.client.Minecraft;

public abstract class HUDModBase
{
	protected static final Minecraft mc = Minecraft.getMinecraft();
	protected static final net.minecraft.client.renderer.RenderItem itemRenderer = mc.getRenderItem();
	
	//We can't move the static variable Enabled to this base mod because then if one mod sets it to false
	//then ALL mods will be set to false
}