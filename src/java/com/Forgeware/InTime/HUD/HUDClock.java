package com.Forgeware.InTime.HUD;

import net.minecraft.entity.player.EntityPlayer;

import com.Forgeware.InTime.Handlers.ClockHandler;
import com.Forgeware.InTime.Time.Time;
import com.Forgeware.InTime.Time.TimeManager;

public class HUDClock {

	public static String CalculateMessageForInfoLine(String infoLineMessage, EntityPlayer player) {
		Time t = TimeManager.Query(player.getUniqueID());
		
		if(t != null && !t.isDepleted())
			return ClockHandler.Clock(TimeManager.Query(player.getUniqueID()));
		else return "SHREKT";
	}

}
