package com.Forgeware.InTime.Commands;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

import com.Forgeware.InTime.Time.TimeManager;

public class CommandPauseTime implements ICommand {

	@SuppressWarnings("rawtypes")
	private final List aliases;
	
	@SuppressWarnings("rawtypes")
	public CommandPauseTime(){
		aliases = new ArrayList();
	}
	
	public int compareTo(ICommand arg0) {
		return 0;
	}

	public String getCommandName() {
		return "pausetime";
	}

	public String getCommandUsage(ICommandSender sender) {
		return "/pausetime (player)";
	}

	@SuppressWarnings("unchecked")
	public List<String> getCommandAliases() {
		// TODO Auto-generated method stub
		return aliases;
	}

	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
World world = sender.getEntityWorld();
		
		if(world.isRemote){
			
		}else{
			if(sender instanceof EntityPlayer && canCommandSenderUseCommand(sender)){
				if(args.length == 0){
					if(TimeManager.getPaused(((EntityPlayer)sender).getUniqueID())){
						sender.addChatMessage(new TextComponentString("Unpaused Time"));
						TimeManager.PauseTime(((EntityPlayer)sender).getUniqueID(), false);
						TimeManager.Query(((EntityPlayer)sender).getUniqueID()).setTick(world.getWorldTime());
					}else{
						sender.addChatMessage(new TextComponentString("Paused Time"));
						TimeManager.PauseTime(((EntityPlayer)sender).getUniqueID(), true);
					}
				}else if(args.length == 1){
					if(getUserID(sender, args, 0) != null){
						if(TimeManager.getPaused(getUserID(sender, args, 0))){
							sender.addChatMessage(new TextComponentString("Unpaused Time"));
							TimeManager.PauseTime(getUserID(sender, args, 0), false);	
							TimeManager.Query(getUserID(sender, args, 0)).setTick(world.getWorldTime());
						}else{
							sender.addChatMessage(new TextComponentString("Paused Time"));
							TimeManager.PauseTime(getUserID(sender, args, 0), true);
						}
						return ;
						
					}
				}
			}		
		}
	}
	

	public boolean canCommandSenderUseCommand(ICommandSender sender) {
		if(sender instanceof EntityPlayer){
			if(sender.canCommandSenderUseCommand(0, getCommandName())){
				return true;
			}else{
				return false;
				}
			
		}else{
			return true;
		}
	
	}

	public List<String> addTabCompletionOptions(ICommandSender sender, String[] args, BlockPos pos) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isUsernameIndex(String[] args, int index) {
		
		return false;
	}

	public UUID getUserID(ICommandSender sender, String [] args, int augment) {
		
		if(sender instanceof EntityPlayer){
			World world = ((EntityPlayer)sender).worldObj;
			for(int i = 0; i < world.playerEntities.size(); i++){
				if(world.playerEntities.get(i).equals(world.getPlayerEntityByName(args[augment]))){
					return ((EntityPlayer)world.getPlayerEntityByName(args[augment])).getUniqueID();
				}else{
					sender.addChatMessage(new TextComponentString("No User with the name: " + args[augment]));
				}
			}
			
		}else{
			sender.addChatMessage(new TextComponentString("Console has no UUID"));
		}
		return null;
	}

	public void execute(MinecraftServer server, ICommandSender sender,
			String[] args) throws CommandException {
		// TODO Auto-generated method stub
		
	}

	public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
		// TODO Auto-generated method stub
		return false;
	}

	public List<String> getTabCompletionOptions(MinecraftServer server,
			ICommandSender sender, String[] args,
			net.minecraft.util.math.BlockPos pos) {
		// TODO Auto-generated method stub
		return null;
	}
	

}
