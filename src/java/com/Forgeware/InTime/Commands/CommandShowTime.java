package com.Forgeware.InTime.Commands;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.Forgeware.InTime.Time.Time;
import com.Forgeware.InTime.Time.TimeManager;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

public class CommandShowTime implements ICommand{

	@SuppressWarnings("rawtypes")
	private final List aliases;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public CommandShowTime(){
		aliases = new ArrayList();
		aliases.add("gettime");
	}
	
	public int compareTo(ICommand arg0) {
		return 0;
	}

	public String getCommandName() {
		return "showtime";
	}

	public String getCommandUsage(ICommandSender sender) {
		return "/showtime (player)";
	}

	@SuppressWarnings("unchecked")
	public List<String> getCommandAliases() {
		
		return this.aliases;
	}

	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
		World world = sender.getEntityWorld();
		
		if(world.isRemote){
			
		}else{
			if(sender instanceof EntityPlayer && canCommandSenderUseCommand(sender)){
				if(args.length == 0){
					Time time = TimeManager.Query(((EntityPlayer)sender).getUniqueID());
					sender.addChatMessage(new TextComponentString("You Have " + time.getYears() + " Years, " + time.getDays() + " Days, " + time.getHours() + " Hours, " + time.getMinutes() + " Minutes, " + time .getSeconds() + " Seconds Left till you perish"));
				}else if(args.length == 1){
					Time time = TimeManager.Query(getUserID(sender, args, 0));
					sender.addChatMessage(new TextComponentString(args[0] + " Has " + time.getYears() + " Years, " + time.getDays() + " Days, " + time.getHours() + " Hours, " + time.getMinutes() + " Minutes, " + time .getSeconds() + " Seconds Left till they perish"));
				}
			}		
		}
	}
	

	public boolean canCommandSenderUseCommand(ICommandSender sender) {
		if(sender instanceof EntityPlayer){
			if(sender.canCommandSenderUseCommand(0, getCommandName())){
				return true;
			}else{
				return false;
				}
			
		}else{
			return true;
		}
	
	}

	public List<String> addTabCompletionOptions(ICommandSender sender, String[] args, BlockPos pos) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isUsernameIndex(String[] args, int index) {
		
		return false;
	}

	public UUID getUserID(ICommandSender sender, String [] args, int augment) {
		
		if(sender instanceof EntityPlayer){
			World world = ((EntityPlayer)sender).worldObj;
			for(int i = 0; i < world.playerEntities.size(); i++){
				if(world.playerEntities.get(i).equals(world.getPlayerEntityByName(args[augment]))){
					return ((EntityPlayer)world.getPlayerEntityByName(args[augment])).getUniqueID();
				}else{
					sender.addChatMessage(new TextComponentString("No User with the name: " + args[augment]));
				}
			}
			
		}else{
			sender.addChatMessage(new TextComponentString("Console has no UUID"));
		}
		return null;
	}

	public void execute(MinecraftServer server, ICommandSender sender,
			String[] args) throws CommandException {
		// TODO Auto-generated method stub
		
	}

	public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
		// TODO Auto-generated method stub
		return false;
	}

	public List<String> getTabCompletionOptions(MinecraftServer server,
			ICommandSender sender, String[] args,
			net.minecraft.util.math.BlockPos pos) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
