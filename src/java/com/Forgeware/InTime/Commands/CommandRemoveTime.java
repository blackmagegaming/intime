package com.Forgeware.InTime.Commands;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

import org.apache.commons.lang3.math.NumberUtils;

import com.Forgeware.InTime.Time.Time;
import com.Forgeware.InTime.Time.TimeManager;

public class CommandRemoveTime implements ICommand{

	@SuppressWarnings("rawtypes")
	public final List aliases;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public CommandRemoveTime(){
		aliases = new ArrayList();
		aliases.add("taketime");
	}
	
	public int compareTo(ICommand arg0) {
		return 0;
	}

	public String getCommandName() {
		return "removetime";
	}

	public String getCommandUsage(ICommandSender sender) {
		return "/removetime [minutes] (hours) (days) (years) (player)";
	}

	@SuppressWarnings("unchecked")
	public List<String> getCommandAliases() {
		
		return this.aliases;
	}

	public void processCommand(ICommandSender sender, String[] args) throws CommandException {
		World world = sender.getEntityWorld();
		
		if(world.isRemote){
			
		}else{
			if(sender instanceof EntityPlayer && canCommandSenderUseCommand(sender)){
				if(args.length == 0 || !NumberUtils.isNumber(args[0])){
					sender.addChatMessage(new TextComponentString("Invalid Augments. Correct Usage is: /removetime [minutes] (hours) (days) (years) (player)"));
				}else{
					if(args.length >= 1){
						if(args.length >= 2){
							if(args.length >= 3){
								if(args.length >= 4){
									if(args.length >= 5){
										if(getUserID(sender, args, 4) != null){
											TimeManager.DecreaseTime(getUserID(sender, args, 4), new Time(0, Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3])));
											sender.addChatMessage(new TextComponentString("Removed " + args[0] + " Minutes, " + args[1] + " Hours, " + args[2] + " Days, " + args[3] + " Years from " + args[4]));
											return ;
										}
										
									}else if(!NumberUtils.isNumber(args[3])){
										TimeManager.DecreaseTime(getUserID(sender, args, 3), new Time(0, Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]), 0));
										sender.addChatMessage(new TextComponentString("Removed " + args[0] + " Minutes, " + args[1] + " Hours, " + args[2] + " Days from " + args[3]));
										return ;
									}else{
										TimeManager.DecreaseTime(((EntityPlayer)sender).getUniqueID(), new Time(0, Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]), Integer.parseInt(args[3])));
										sender.addChatMessage(new TextComponentString("Removed " + args[0] + " Minutes, " + args[1] + " Hours, " + args[2] + " Days, " + args[3] + " Years"));
										return ;
									}
								}else if(!NumberUtils.isNumber(args[2])){
									TimeManager.DecreaseTime(getUserID(sender, args, 2), new Time(0, Integer.parseInt(args[0]), Integer.parseInt(args[1]), 0, 0));
									sender.addChatMessage(new TextComponentString("Removed " + args[0] + " Minutes, " + args[1] + " Hours from " + args[2]));
									return ;
								}else{
									TimeManager.DecreaseTime(((EntityPlayer)sender).getUniqueID(), new Time(0, Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]), 0));
									sender.addChatMessage(new TextComponentString("Removed " + args[0] + " Minutes, " + args[1] + " Hours, " + args[2] + " Days"));
									return ;
								}
							}else if(!NumberUtils.isNumber(args[1])){
								TimeManager.DecreaseTime(getUserID(sender, args, 1), new Time(0, Integer.parseInt(args[0]), 0, 0, 0));
								sender.addChatMessage(new TextComponentString("Removed " + args[0] + " Minutes from " + args[1]));
								return ;
							}else{
								TimeManager.DecreaseTime(((EntityPlayer)sender).getUniqueID(), new Time(0, Integer.parseInt(args[0]), Integer.parseInt(args[1]), 0, 0));
								sender.addChatMessage(new TextComponentString("Removed " + args[0] + " Minutes, " + args[1] + " Hours"));
								return ;
							}
						}else{
							TimeManager.DecreaseTime(((EntityPlayer)sender).getUniqueID(), new Time(0, Integer.parseInt(args[0]), 0, 0, 0));
							sender.addChatMessage(new TextComponentString("Removed " + args[0] + " Minutes"));
							return ;
						}
					}
				}
			}		
		}
	}
	

	public boolean canCommandSenderUseCommand(ICommandSender sender) {
		if(sender instanceof EntityPlayer){
			if(sender.canCommandSenderUseCommand(0, getCommandName())){
				return true;
			}else{
				return false;
				}
			
		}else{
			return true;
		}
	
	}

	public List<String> addTabCompletionOptions(ICommandSender sender, String[] args, BlockPos pos) {
		// TODO Auto-generated method stub
		return null;
	}

	public boolean isUsernameIndex(String[] args, int index) {
		
		return false;
	}

	public UUID getUserID(ICommandSender sender, String [] args, int augment) {
		
		if(sender instanceof EntityPlayer){
			World world = ((EntityPlayer)sender).worldObj;
			for(int i = 0; i < world.playerEntities.size(); i++){
				if(world.playerEntities.get(i).equals(world.getPlayerEntityByName(args[augment]))){
					return ((EntityPlayer)world.getPlayerEntityByName(args[augment])).getUniqueID();
				}else{
					sender.addChatMessage(new TextComponentString("No User with the name: " + args[augment]));
				}
			}
			
		}else{
			sender.addChatMessage(new TextComponentString("Console has no UUID"));
		}
		return null;
	}

	public void execute(MinecraftServer server, ICommandSender sender,
			String[] args) throws CommandException {
		// TODO Auto-generated method stub
		
	}

	public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
		// TODO Auto-generated method stub
		return false;
	}

	public List<String> getTabCompletionOptions(MinecraftServer server,
			ICommandSender sender, String[] args,
			net.minecraft.util.math.BlockPos pos) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
